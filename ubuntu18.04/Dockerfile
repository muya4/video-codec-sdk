ARG repository
ARG flavor

FROM ${repository}:11.2.2-${flavor}-ubuntu18.04

ENV VIDEOSDK_VERSION 11.0.10
ENV CUDA_PKG_VERSION 11-2
LABEL com.nvidia.videosdk.version="${VIDEOSDK_VERSION}"

ENV LIBRARY_PATH /usr/local/cuda/lib64/stubs${LIBRARY_PATH:+:${LIBRARY_PATH}}

ENV NVIDIA_DRIVER_CAPABILITIES ${NVIDIA_DRIVER_CAPABILITIES},video

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        cuda-cudart-dev-${CUDA_PKG_VERSION} \
        cuda-driver-dev-${CUDA_PKG_VERSION} && \
    apt-get install -y --no-install-recommends unzip curl

WORKDIR /opt/nvidia/video-sdk
COPY Video_Codec_SDK_${VIDEOSDK_VERSION}.zip /opt/nvidia/video-sdk

RUN unzip -j Video_Codec_SDK_${VIDEOSDK_VERSION}.zip \
          Video_Codec_SDK_${VIDEOSDK_VERSION}/Interface/cuviddec.h \
          Video_Codec_SDK_${VIDEOSDK_VERSION}/Interface/nvcuvid.h \
          Video_Codec_SDK_${VIDEOSDK_VERSION}/Interface/nvEncodeAPI.h \
          -d /usr/local/cuda/include && \
    unzip -j Video_Codec_SDK_${VIDEOSDK_VERSION}.zip \
          Video_Codec_SDK_${VIDEOSDK_VERSION}/Lib/linux/stubs/x86_64/libnvcuvid.so \
          Video_Codec_SDK_${VIDEOSDK_VERSION}/Lib/linux/stubs/x86_64/libnvidia-encode.so \
          -d /usr/local/cuda/lib64/stubs && \
    apt-get purge --autoremove -y unzip curl && \
    rm -rf /var/lib/apt/lists/*

# pkg-config files
COPY usr /usr
